# Project Game

Repository for The Project Game (IO2 2018 summer)

## Getting Started

The project is built on .NET Core framework and uses xUnit testing framework.

### Prerequisites

What things you need to install the software and how to install them

```
.NET Core 2.0

code editor (VSCode is a good starting point)
```

### Installing

You have to simply do restore and build in entry point project folder.
 

```
dotnet restore
dotnet run
```


End with an example of getting some data out of the system or using it for a little demo

### Running the tests
 

```
dotnet test
```

### Coding style

We use LF as line endings in the repo. You can configure git-scm to fetch as-is and commit with LFs.

As for coding style in the project:
[Microsoft C# coding convention](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/inside-a-program/coding-conventions)


### Git flow

We use git flow as our convention for source control.
Feature branches are always made from develop branch and after story's Definition of Done is made we merge them (-no-ff!) to develop.
Fix branches can be made either from develop or master, but typically we won't use master because of nature of the project.

For user stories and tasks (such as: building communication or implementing moves on the board) we make feature branches with names:

```
feature/communication
feature/board-moves
```

For bugs and minor fixes we make fix branches like:

```
fix/handle-wrong-xml
```

## Authors

* **Mateusz Wojtczak** - *Scrummaster*
* **Bartek Chechliński** - *Product Owner*
* **Damian Baciur** - *Developer*
* **Damian Cudzik** - *Developer*
* **Kamil Gacan** - *Developer*

## Acknowledgments

* This is a study group project
